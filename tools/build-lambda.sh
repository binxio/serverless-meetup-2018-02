#!/usr/bin/env bash
set -e

if [ "$DEBUG" = "true" ] ; then
  set -x
fi

if [ $# -ne 1 ]; then
  echo "USAGE: ${0} <path to directory containing lambda code>" >&2
  exit 1;
fi

LAMBDA_NAME="${1}"

pushd ${LAMBDA_NAME}
# Install deps
docker run --rm -ti -v ${PWD}:/var/task lambci/lambda:build-python3.6 pip install -t .packages -r requirements.txt
# Test
# docker-compose -f docker-compose.it.yml run app
if [ -d .packages ]; then
  # Delete files we don't need
  docker run --rm -ti -v ${PWD}:/var/task lambci/lambda:build-python3.6 sh -c 'find .packages -type d -iname "tests" | xargs --no-run-if-empty rm -r'
  docker run --rm -ti -v ${PWD}:/var/task lambci/lambda:build-python3.6 sh -c "find .packages -type d -iname '__pycache__' | xargs --no-run-if-empty rm -r"
  # Create zip file to deploy to Lambda
  pushd .packages
  docker run --rm -ti -v ${PWD}:/var/task lambci/lambda:build-python3.6 zip -9 -r lambda.zip .
  mv lambda.zip ../lambda.zip
  popd
fi
docker run --rm -ti -v ${PWD}:/var/task lambci/lambda:build-python3.6 zip -9 -r lambda.zip . -x \*.packages\*

popd
