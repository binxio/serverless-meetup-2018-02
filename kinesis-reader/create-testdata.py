import testdata
import json
import boto3
import os

StreamName = os.getenv("STREAM_NAME")

class Users(testdata.DictFactory):
    firstname = testdata.FakeDataFactory('firstName')
    lastname = testdata.FakeDataFactory('lastName')
    age = testdata.RandomInteger(10, 30)
    gender = testdata.RandomSelection(['female', 'male'])

kinesis = boto3.client('kinesis')

for user in Users().generate(200): # let say we only want 10 users
    print(user)
    bla = kinesis.put_record(StreamName=StreamName,
    Data=json.dumps(user),
    PartitionKey=user['lastname'])
