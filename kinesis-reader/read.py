import json
import boto3
import time

kinesis = boto3.client('kinesis')

shard_id = 'shardId-000000000001' #we only have one shard!
shard_iterator = kinesis.get_shard_iterator(StreamName="BotoDemo", ShardId=shard_id)["ShardIterator"]

while 1==1:
    out = kinesis.get_records(ShardIterator=shard_iterator, Limit=2)
    shard_iterator = out["NextShardIterator"]
    print out;
    time.sleep(1)
