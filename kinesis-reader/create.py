import boto3
import json

client = boto3.client('kinesis')

create_response = client.create_stream(StreamName='BotoDemo', ShardCount=1)
print(create_response)

describe_response = client.describe_stream(StreamName='BotoDemo')
print(describe_response)

print(json.dumps(client.list_streams()['StreamNames']))
