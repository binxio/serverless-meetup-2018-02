# serverless-architecture-meetup

It's now time to prepare your computer for some AWS interaction if you haven't done that previously.

You will need:

- [An AWS Account](https://portal.aws.amazon.com/gp/aws/developer/registration/index.html), the Free Tier enables you to gain free, hands-on experience.
- [The AWS command line interface](https://docs.aws.amazon.com/cli/latest/userguide/installing.html)

Optionally you'll need:

- [HashiCorp Terraform to Write, Plan, and Create Infrastructure as Code](https://www.terraform.io/)
- [SAM local for some local testing](https://github.com/awslabs/aws-sam-local)

[Some reading on SAM](https://github.com/awslabs/serverless-application-model)

Most of it can also be installed with [brew](https://brew.sh/) on Mac

Questions? Please feel free to ask us.
