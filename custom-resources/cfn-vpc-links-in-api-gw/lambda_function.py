from __future__ import print_function
import json
import boto3
import requests
import time

def create(client, event):
    resource_name = event['ResourceProperties']['Name']
    vpc_links_response = client.get_vpc_links()
    vpc_link_count = sum(1 for v in vpc_links_response['items'] if v.get('name') == resource_name)
    if vpc_link_count > 0:
        respond_cloudformation(event, "FAILED", physical_resource_id='na', reason="A VPC Link already exists: " + resource_name)
    else:
        create_vpc_link_response = client.create_vpc_link(
            name=resource_name,
            description='',
            targetArns=[event['ResourceProperties']['TargetArn']]
        )
        print(json.dumps(create_vpc_link_response)) 
        vpc_link_id = create_vpc_link_response.get('id', 'na')
        respond_cloudformation(event, "SUCCESS", physical_resource_id=vpc_link_id, reason="VPC link (" + vpc_link_id + ") creation started", data={ "ConnectionId": vpc_link_id })

def handler(event, context):
    try:
        client = boto3.client('apigateway')
        print(json.dumps(event))
        if event['RequestType'] == 'Create':
            print("Creating VPC Link")
            create(client, event)
        elif event['RequestType'] == 'Update':
            print("Update")
            respond_cloudformation(event, "SUCCESS", physical_resource_id = event.get('PhysicalResourceId', 'na'),data={ "ConnectionId": event.get('PhysicalResourceId', 'na') })
        elif event['RequestType'] == 'Delete':
            vpcLinkId = event['PhysicalResourceId']
            if vpcLinkId == 'na':
                respond_cloudformation(event, "SUCCESS", physical_resource_id = vpcLinkId, reason='Nothing to delete')
            else:
                try:
                    print('Removing ' + vpcLinkId)
                    response = client.delete_vpc_link(vpcLinkId=vpcLinkId)
                    print("client.delete_vpc_link response")
                    print(json.dumps(response))
                except Exception as e:
                    respond_cloudformation(event=event, status="SUCCESS", physical_resource_id=event.get('PhysicalResourceId', 'na'), reason=str(e))

                try:
                    while True:
                        get_vpc_response = client.get_vpc_link(vpcLinkId=vpcLinkId)
                        time.sleep(1)
                except Exception as e:
                    respond_cloudformation(event, "SUCCESS", physical_resource_id = vpcLinkId)
                
    except Exception as e:
        print(str(e))
        respond_cloudformation(event=event, status="FAILED", physical_resource_id=event.get('PhysicalResourceId', 'na'), reason=str(e))

def respond_cloudformation(event, status, physical_resource_id, data=None, reason='See the details in CloudWatch Log Stream'):
    responseBody = {
        'Status': status,
        'Reason': reason,
        'PhysicalResourceId': physical_resource_id,
        'StackId': event['StackId'],
        'RequestId': event['RequestId'],
        'LogicalResourceId': event['LogicalResourceId'],
        'Data': data
    }
    print('Response = ' + json.dumps(responseBody))
    requests.put(event['ResponseURL'], data=json.dumps(responseBody))

# This is the test harness
if __name__ == '__main__':
    delete_request = {
        'StackId': 'arn:aws:cloudformation:us-west-2:accountgoeshere:stack/sample-stack/stackidgoeshere',
        'ResponseURL': 'https://test.com',
        'ResourceProperties': {
            'Name': 'furby-test',
            'TargetArn': 'arn:aws:elasticloadbalancing:eu-west-1:111111111111111:loadbalancer/net/furby-t-NLB-48AGX4T9JW94/73d0ddd5c6099c14'
        },
        'RequestType': 'Delete',
        'ServiceToken': 'lambdaarn',
        'ResourceType': 'Custom::Lookup',
        'RequestId': 'sampleid',
        'LogicalResourceId': 'CUSTOMLOOKUP',
        'PhysicalResourceId': 'nfj83y'
    }
    create_request = {
        'StackId': 'arn:aws:cloudformation:us-west-2:accountgoeshere:stack/sample-stack/stackid',
        'ResponseURL': 'https://test.com',
        'ResourceProperties': {
            'Name': 'furby-test',
            'TargetArn': 'arn:aws:elasticloadbalancing:eu-west-1:111111111111111:loadbalancer/net/furby-t-NLB-48AGX4T9JW94/73d0ddd5c6099c14'
        },
        'RequestType': 'Create',
        'ServiceToken': 'arn:aws:lambda:us-west-2:accountgoeshere:function:lambdafunction',
        'ResourceType': 'Custom::Lookup',
        'RequestId': 'ramdonid',
        'LogicalResourceId': 'CUSTOMLOOKUP'
    }
    handler(create_request, None)
    handler(delete_request, None)
