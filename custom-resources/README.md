Custom resource to create a VPC Link to a NLB in an API Gateway


#Syntax

YAML

```
  VPCLink:
    Type: Custom::VpcLink
    Properties:
      ServiceToken: !ImportValue cf-create-vpc-link-arn
      Name: String
      TargetArn: String

```
#Properties

### ServiceToken

Arn to the Lambda function created to implement the resource logic.


### Name:

Pretty name to give the VPC Link.


### TargetArn

ARN of the loadbalancer to point to.

#Return Values
`!GetAtt` returns a value for the following attributes.

`ConnectionId` the ID taht can be used to point traffic to.


#Examples:
```
  VPCLink:
    Type: Custom::VpcLink
    Properties:
      ServiceToken: !ImportValue cf-create-vpc-link-arn
      Name: furby-it-test
      TargetArn: !Ref NLB

```
