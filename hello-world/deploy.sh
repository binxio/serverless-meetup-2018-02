#!/bin/bash

aws cloudformation package --template-file template-input.yml --s3-bucket lambda-artifacts-thijs --output-template-file template-output.yml

aws cloudformation deploy --template-file template-output.yml --stack-name lambda-thijs --capabilities CAPABILITY_IAM
