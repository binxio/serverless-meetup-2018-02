import os
import requests
import json

# IRL do this more safe!
# HOOK_URL = "https://" + boto3.client('kms').decrypt(CiphertextBlob=b64decode(ENCRYPTED_HOOK_URL))['Plaintext']

# The Slack channel to send a message to stored in the slackChannel environment variable
SLACK_CHANNEL = os.environ['SlackChannel']
HOOK_URL = os.environ['WebhookURL']

def lambda_handler(event, context):
    print(json.dumps(event))
    slack_message = {
        'channel': SLACK_CHANNEL,
        'text': "%s" % ("Hello Serverless World!")
    }
    print(slack_message)
    with requests.post(HOOK_URL, data=json.dumps(slack_message), headers={'content-type': 'application/json'}) as response:
        print(response.text)


# Test:
if __name__ == "__main__":
    lambda_handler({}, {})
