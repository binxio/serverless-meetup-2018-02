# serverless-architecture-meetup

Welcome to the AWS Meetup, this is the serverless edition.

Please take some time to get settled and your [setup](https://gitlab.com/binxio/serverless-meetup-2018-02/blob/master/setup.md) running.

To start off this evening's session we would like you to pick an example project or make one up yourselves.

## Example projects

Please bear in mind that the following snippets are not plug-and-play. You'll need to do some tweaking to get them going (this is where the fun is).

### Custom AWS Metrics

AWS offers various out of the box metrics that can be used. However, the list of metrics is quite long there always things you might miss. This example shows how to deploy a custom metric with Terraform.

[Link to examples](https://gitlab.com/binxio/serverless-meetup-2018-02/blob/master/custom-metrics) 

### Handle Kinesis

Kinesis stream can be an input of a Lambda function. You will find some basic scripts to kickstart the exploration of the possibilities.

[Link te example](https://gitlab.com/binxio/serverless-meetup-2018-02/blob/master/kinesis-reader)

### Logs forwarder

For a customer project, we wanted the logs in Logz.io. We use to forward logs from CloudWatch Logs to Logz.io.

[Link to example](https://gitlab.com/binxio/serverless-meetup-2018-02/blob/master/logzio-forwarder)

### Push messages to Slack

We all love slack, with this function you can send a message to the push API. Extendable to send all kinds of messages.

[Link to examples](https://gitlab.com/binxio/serverless-meetup-2018-02/blob/master/hello-world)

Can be extended to ECS reporter. When running ECS it can be useful to know if containers are being restarted. We can subscribe to the CloudWatchEvent of aws.ecs.
