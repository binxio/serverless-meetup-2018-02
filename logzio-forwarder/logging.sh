#!/bin/bash

FUNCTION=$1
FUNCTION_ARN=$2
ACCOUNT_ID=$3

## Delete all filters for function
aws lambda get-policy --function-name $FUNCTION --query Policy --output text | jq .Statement[].Sid --raw-output | while read STATEMENT_ID
do
  aws lambda remove-permission --function-name $FUNCTION --statement-id $STATEMENT_ID
done

# Get all to-be monitored log groups
MONITOR_SOURCES=$(aws logs describe-log-groups | jq .logGroups[].logGroupName --raw-output | grep -E 'service$|site$|task$')

# Delete existing subsciptions
for LOG_GROUP in ${MONITOR_SOURCES}
do
  FILTER=$(aws logs describe-subscription-filters --log-group-name $LOG_GROUP | jq .subscriptionFilters[].filterName --raw-output)
  if [[ "$FILTER" == "" ]]; then
    echo "no filter found for ${LOG_GROUP}"
  else
    echo delete-subscription-filter --log-group-name $LOG_GROUP --filter-name $FILTER
    aws logs delete-subscription-filter --log-group-name $LOG_GROUP --filter-name $FILTER
  fi
done

# Add permissions for logging
for LOG_GROUP in ${MONITOR_SOURCES}
do
  echo "Setting up permission $LOG_GROUP"
  aws lambda add-permission --function-name ${FUNCTION} --statement-id ${LOG_GROUP}-$(uuidgen) --source-account ${ACCOUNT_ID} --source-arn arn:aws:logs:eu-west-1:${ACCOUNT_ID}:log-group:${LOG_GROUP}:* --action "lambda:InvokeFunction" --principal logs.eu-west-1.amazonaws.com
done

sleep 2

# Add subscriptions for logging
for LOG_GROUP in ${MONITOR_SOURCES}
do
  echo "Setting up subscription ${LOG_GROUP}"
  aws logs put-subscription-filter --log-group-name ${LOG_GROUP} --filter-name ${LOG_GROUP}-filter --filter-pattern '' --destination-arn ${FUNCTION_ARN}
done
