import time
import boto3
import json
from time import gmtime, strftime

client = boto3.client('logs')

response = client.describe_log_streams(
    logGroupName='ThijsTest',
    logStreamNamePrefix='Stream1'
)

print(response)
sequenceToken = response['logStreams'][0]['uploadSequenceToken']
#print(sequenceToken)
events = []

for i in range(1000):
  message = "testbericht_{}".format(i)
  print message
  datum=strftime("%Y-%m-%dT%H:%M:%S.000+02:00", gmtime())
  events.append({ "timestamp": int(time.time()*1000),
        "message": json.dumps({
          "@timestamp": datum,
          "level": "DEBUG",
          "X-B3-TraceId": "70817aeea569c710",
          "X-B3-SpanId": "70817aeea569c710",
          "X-B3-ParentSpanId": "",
          "X-Span-Export": "false",
          "message": message,
          "thread_name": "http-nio-8080-exec-9",
          "logger_name": "o.s.w.filter.CommonsRequestLoggingFilter",
          "stack_trace": ""
        })
      })

response = client.put_log_events(
    logGroupName='ThijsTest',
    logStreamName='Stream1',
    logEvents=events,
    sequenceToken=sequenceToken
  )
