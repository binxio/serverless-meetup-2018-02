import logging
import logging.config
try:
    import simplejson as json
except ImportError:
    import json
import gzip
import os
from StringIO import StringIO
from urllib2 import urlopen

# Say i have saved my configuration under ./myconf.conf
logging.config.fileConfig('logging.conf')
logger = logging.getLogger('superAwesomeLogzioLogger')
logger.info('Loading function')

url = os.environ['LOGZ_IO_URL']
environment = os.environ['ENVIRONMENT']

def lambda_handler(event, context):
    # For debugging so you can see raw event format.
    print('Here is the event:')
    print(json.dumps(event))

    body = json.dumps({ "message": event["detail"]["group"] + " " + event["detail"].get("stoppedReason", "na")
      , "environment": environment
      , "stoppedReason": event["detail"].get("stoppedReason", "na")
      , "group": event["detail"]["group"]
      , "taskArn": event["detail"]["taskArn"]
      , "group": event["detail"]["group"]
      , "lastStatus": event["detail"]["lastStatus"]
      , "containerInstanceArn": event["detail"]["containerInstanceArn"]
      , "updatedAt": event["detail"]["updatedAt"]
    } )+ "\n"

    response = urlopen(url, timeout=10, data=body).read()

    return
