import boto3
import os
import time
import logging
import sys

LOGGER = None
CLUSTER = 'default'

def log_configure():
    """ Configure and return main logger """
    logger = logging.getLogger('main')
    logger_channel = logging.StreamHandler(sys.stdout)
    logger_channel.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s')
    logger_channel.setFormatter(formatter)
    logger.addHandler(logger_channel)
    logger.setLevel(logging.DEBUG)
    return logger

def send_cloudwatch_metric(name, value=0, unit='Count'):
    """ <name> represents the metric name and can be one of Errors, Invocations, Successes, RowCount or <phase>Duration.
        Each metric will have tasknames as dimensions. Format: oracle-<offload_schema>-<offload_table>

        <unit> specifies the data unit, e.g. total count, count of items per second, etc.
    """
    client = boto3.client('cloudwatch')

    client.put_metric_data(
        Namespace='DataPlatform',
        MetricData=[
            {
                'MetricName': name,
                'Dimensions': [
                    {
                        'Name': 'ECS',
                        'Value': CLUSTER
                    }
                ],
                'Timestamp': int(time.time()),
                'Value': value,
                'Unit': unit
            }
        ]
    )

def lambda_handler(event, context):
    LOGGER = log_configure()

    ecs_client = boto3.client('ecs')
    container_instances = ecs_client.list_container_instances(cluster=CLUSTER)['containerInstanceArns']

    containerCountTotal = 0
    for containerInstance in container_instances:
        containerCountTotal += len(ecs_client.list_tasks(cluster=CLUSTER, containerInstance=containerInstance)['taskArns'])

    LOGGER.info("ConcurrentContainersCount: {}".format(str(containerCountTotal)))
    send_cloudwatch_metric(
        name='ConcurrentContainersCount',
        value=containerCountTotal)

    LOGGER.info("ConcurrentInstancesCount: {}".format(str(len(container_instances))))
    send_cloudwatch_metric(
        name='ConcurrentInstancesCount',
        value=len(container_instances))


    LOGGER.info("AverageContainersPerInstance: {}".format(str(containerCountTotal/len(container_instances))))
    send_cloudwatch_metric(
        name='AverageContainersPerInstance',
        value=containerCountTotal/len(container_instances))
