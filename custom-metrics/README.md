# Custom AWS Metrics

AWS offers various out of the box metrics that can be used for i.e. auto-scalers. One of the thins that we missed was to autoscale based on the amount of ENI's used per instance. This metric by itself, is not retrievable but, in our case, we only have containers with a custom ENI, so this suited our needs.

## Build Lambda

If the Lambda requires specific libraries, the Lambda needs to be packaged. The `build-lambda.sh` script inside the `tools` folder, is an example on how to do achieve this. Here we will install all required libraries inside the `.packages` folder and put this next the Lambda code. This way we have one zip file that we can ship to AWS.

## Set-up Infrastructure

To get the Lambda running on AWS, you will need several components. An example is illustrated below. Please be aware that these parts are taken out of a production environment and therefore need some customisation for your needs.

### Terraform

#### KMS Key

```
resource "aws_kms_key" "kms_key" {
  description = "This KMS key is used for encrypting data to/from the Lambda"
}

resource "aws_kms_alias" "kms_key_alias" {
  name          = "alias/my-awesome-lambda-key"
  target_key_id = "${aws_kms_key.kms_key.key_id}"
}

```

#### Policies

```
data "aws_iam_policy_document" "lambda_execute" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "lambda_iam_role" {
  name               = "lambda-execute-role"
  assume_role_policy = "${data.aws_iam_policy_document.lambda_execute.json}"
}
```

#### Lambda

```
data "archive_file" "lambda_zip" {
  type        = "zip"
  source_dir  = "${path.module}/source"
  output_path = "${path.module}/source/lambda.zip"
}

resource "aws_lambda_function" "lambda_func" {
  function_name = "your-lambda-name"
  role          = "${aws_iam_role.lambda_iam_role.arn}"

  handler          = "lambda_function.lambda_handler"
  runtime          = "python3.6"

  description = "Description for your awesome Lambda"
  timeout     = "300"
  memory_size = "128"

  kms_key_arn = "${aws_kms_alias.kms_key_alias.arn}"
}
```

#### Cloudwatch scheduler

If you want the metrics to be gathered every 5 minutes, you could use the AWS CloudWatch eventrule feature.

```
resource "aws_cloudwatch_event_rule" "every_five_minutes" {
  name                = "my-lambda-metric-every-5-minutes"
  description         = "Fetch metrics every 5 minutes"
  schedule_expression = "rate(5 minutes)"
}

resource "aws_cloudwatch_event_target" "check_every_five_minutes" {
  rule      = "${aws_cloudwatch_event_rule.every_five_minutes.name}"
  target_id = "my-lambda-metric-every-5-minutes"
  arn       = "${aws_lambda_function.lambda_func.arn}"
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_lambda" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.lambda_func.arn}"
  principal     = "events.amazonaws.com"
  source_arn    = "${aws_cloudwatch_event_rule.every_five_minutes.arn}"
}
```
